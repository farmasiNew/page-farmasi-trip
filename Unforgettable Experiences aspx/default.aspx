<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="LandingPage_Default" %>
<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>Age Reversist</title>
  <link rel="icon" href="/LimitlessMascara/img/favicon.ico" type="image/x-icon">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="/AgeReversist/css/stack-interface.css" rel="stylesheet" type="text/css" media="all">
  <link href="/AgeReversist/css/socicon.css" rel="stylesheet" type="text/css" media="all" />
  <link href="/AgeReversist/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
  <link href="/AgeReversist/css/flickity.css" rel="stylesheet" type="text/css" media="all" />
  <link href="/AgeReversist/css/stack-interface.css" rel="stylesheet" type="text/css" media="all" />
  <link href="/AgeReversist/css/theme-cedar.css" rel="stylesheet" type="text/css" media="all" />
  <link href="/AgeReversist/css/custom.css" rel="stylesheet" type="text/css" media="all" />
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700" rel="stylesheet">
</head>

<body data-smooth-scroll-offset="77">
  <div class="nav-container">
    <div class="via-1600197052799" via="via-1600197052799" vio="Farmasi">
      <div class="bar bar--sm visible-xs">
        <div class="container">
          <div class="row">
            <div class="col-9 col-md-10 text-right">
              <a href="#hero" class="hamburger-toggle" data-toggle-class="#menu2;hidden-xs hidden-sm"> <i
                  class="icon icon--sm stack-interface stack-menu"></i> </a>
            </div>
          </div>
        </div>
      </div>
      <nav id="menu2" class="bar bar-2 hidden-xs bg--dark bar--absolute">
        <div class="container">
          <div class="row">
            <div class="col-lg-5 order-lg-1">
              <div class="bar__module">
                <ul class="menu-horizontal text-left">
                  <li class="dropdown"><a href="https://www.farmasius.com/" title=""> <img
                        src="/LimitlessMascara/img/farmasi-rednwhite.png" alt="" class="img-fluid"></a>

                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </nav>
    </div>
  </div>
  <div class="main-container">
    <a id="hero" class="in-page-link" data-scroll-id="hero"></a>
    <!--<section class="cover imagebg height-100 section--ken-burns text-center">
            <div class="  "> <img alt="background" src="img/lumiradiance01.png"> </div>
            <div class="container pos-vertical-center">
                <div class="row">
                    
                </div>
            </div>
        </section>-->


    <section class="imageblock switchable height-100 d-block img-fluid">
      <!--<div class="imageblock__content col-lg-12 col-sm-12 col-xl-12 col-md-12 pos-right">
                <div class="background-image-holder"> <img alt="image" src="img/lumiradiance01_02.png" class="img-fluid"> </div>
            </div>-->
      <img alt="image" src="/AgeReversist/img/AgeReversist_01a.png" class="img-fluid">
    </section>

    <a id="video" class="in-page-link" data-scroll-id="video"></a>

    <!--<section class="imagebg videobg text-center hidden-md hidden-sm hidden-xs height-90"> <video autoplay="" loop=""
        muted="">
        <source src="video/lumiradiance.webm" type="video/webm">
        <source src="video/lumiradiance.mp4" type="video/mp4">
        <source src="video/lumiradiance.mp4" type="video/ogv">
      </video>
      <div class="background-image-holder"> <img alt="image" src="img/backy3.jpg"> </div>
    </section>
    <a id="video" class="in-page-link"></a>
    <section class="text-center imagebg hidden-md hidden-lg" data-overlay="4">
      <div class="background-image-holder"> </div>
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-lg-10">
            <div class="video-cover border--round">
              <div class="background-image-holder"> </div>
              <div class="video-play-icon"></div> <iframe data-src="https://youtube.com/embed/W2Fc8nzz9K0"
                allowfullscreen="allowfullscreen"></iframe>
            </div>
          </div>
        </div>
      </div>
    </section>-->
    <a id="shop" class="in-page-link" data-scroll-id="shop"></a>
    <section class="imageblock switchable height-100 d-block">
      <!--<div class="imageblock__content col-lg-12 col-md-12 pos-right">
                <div class="background-image-holder"> <img alt="image" src="img/lumiradiance02_02.png" class="img-fluid"> </div>
            </div>-->
      <img alt="image" src="/AgeReversist/img/AgeReversist_02.png" class="img-fluid">
    </section>
    <section class="imageblock switchable height-100 d-block">
      <!--<div class="imageblock__content col-lg-12 col-md-12 pos-right">
                <div class="background-image-holder"> <img alt="image" src="img/lumiradiance03_01.png" class="img-fluid"> </div>
            </div>-->
      <img alt="image" src="/AgeReversist/img/AgeReversist_03.png" class="img-fluid">
    </section>
    <section class="imageblock switchable height-100 d-block">
      <!--<div class="imageblock__content col-lg-12 col-md-12 pos-right">
                <div class="background-image-holder"> <img alt="image" src="img/lumiradiance04_02.png"  class="img-fluid"> </div>
            </div>-->
      <img alt="image" src="/AgeReversist/img/AgeReversist_04.png" class="img-fluid" usemap="#newcomers-2021">
    </section>
    <section class="imageblock switchable height-100 d-block">
      <!--<div class="imageblock__content col-lg-12 col-md-12 pos-right">
                <div class="background-image-holder"> <img alt="image" src="img/lumiradiance04_02.png"  class="img-fluid"> </div>
            </div>-->
      <img alt="image" src="/AgeReversist/img/AgeReversist-2021.01.01-6.png" class="img-fluid">
    </section>
    <a id="about" class="in-page-link" data-scroll-id="about"></a>
    <section class="switchable imagebg switchable--switch space--sm" data-overlay="1"
      style="background-color: #f4f4f4 !important;">
      <!--<div class="background-image-holder"> <img alt="background" src="img/backy3.jpg"> </div>-->
      <div class="container">
        <div class="row justify-content-between">
          <div class="col-lg-12 col-md-12">
            <div class="slider box-shadow-wide border--round" data-arrows="true" data-paging="true" data-timing="10000">
              <ul class="slides" style="background-color:#efeeee">
                <li>
                  <div class="card mb-3">
                    <div class="row no-gutters">
                      <div class="col-sm-4 col-md-4" style="padding: 10px">
                        <img src="/AgeReversist/img/51104139_GelCleanse_400x400.jpg" class="card-img img-fluid" alt="...">
                      </div>
                      <div class="col-sm-8 col-md-8">
                        <div class="card-body">

                          <div
                            style="margin-top:30px;color:#499506;font-family:'Oswald', Arial, 'Helvetica Neue', Helvetica, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                            <div
                              style="line-height: 1.2; font-size: 12px; font-family: 'Oswald', Arial, 'Helvetica Neue', Helvetica, sans-serif; color: #499506; mso-line-height-alt: 14px;">
                              <p
                                style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: left; font-family: Oswald, Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 17px; margin: 0;">
                                <span style="color: #000000;"><span style="font-size: 28px;"><strong>Step 1:
                                    </strong></span></span><span style="color: #008000;"><strong><span
                                      style="font-size: 28px;color: #008000;">CLEANSE</span></strong></span></p>
                              <p
                                style="font-size: 28px; line-height: 1.2; word-break: break-word; text-align: left; font-family: Oswald, Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 34px; margin: 0;">
                                <span style="color: #000000; font-size: 28px;"><strong>Age Reversist Gel
                                    Cleanser</strong></span></p>
                            </div>
                          </div>
                          <div
                            style="color:#393d47;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;line-height:1.5;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                            <div
                              style="line-height: 1.5; font-size: 12px; font-family: 'Lato', Tahoma, Verdana, Segoe, sans-serif; color: #393d47; mso-line-height-alt: 18px;">
                              <p
                                style="line-height: 1.5; word-break: break-word; text-align: left; font-family: Lato, Tahoma, Verdana, Segoe, sans-serif; mso-line-height-alt: 20px; mso-ansi-font-size: 14px; margin: 0;">
                                <span>Washes away dirt, oil, and
                                  other impurities daily, leaving the skin refreshed and smooth. </p>
                              <p
                                style="font-size: 22px; line-height: 1.5; word-break: break-word; text-align: left; font-family: Lato, Tahoma, Verdana, Segoe, sans-serif; mso-line-height-alt: 33px; margin: 0;">
                                <span style="font-size: 22px;"><strong>$16.90</strong></span></p>
                            </div>
                          </div>
                          <div align="left" class="button-container"
                            style="padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                            <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-spacing: 0; border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"><tr><td style="padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px" align="left"><v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="https://www.example.com" style="height:25.5pt; width:112.5pt; v-text-anchor:middle;" arcsize="0%" strokeweight="0.75pt" strokecolor="#000000" fillcolor="#000000"><w:anchorlock/><v:textbox inset="0,0,0,0"><center style="color:#f9f9f9; font-family:Arial, sans-serif; font-size:16px"><![endif]--><a
                              href="/product/dr-c-tuna-age-reversist-gel-cleanser-100-ml/11051"
                              style="-webkit-text-size-adjust: none; text-decoration: none; display: inline-block; color: #f9f9f9; background-color: #000000; border-radius: 0px; -webkit-border-radius: 0px; -moz-border-radius: 0px; width: auto; width: auto; border-top: 1px solid #000000; border-right: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; padding-top: 0px; padding-bottom: 0px; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; text-align: center; mso-border-alt: none; word-break: keep-all;"
                              target="_blank"><span
                                style="padding-left:15px;padding-right:15px;font-size:16px;display:inline-block;"><span
                                  style="font-size: 16px; line-height: 2; word-break: break-word; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; mso-line-height-alt: 32px;color: #FFFFFF;">SHOP
                                  NOW</span></span></a>
                            <!--[if mso]></center></v:textbox></v:roundrect></td></tr></table><![endif]-->
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="card mb-3">
                    <div class="row no-gutters">
                      <div class="col-sm-4 col-md-4" style="padding: 10px">
                        <img src="/AgeReversist/img/1104203_ExfoliatingCleanser_400x400.jpg" class="card-img img-fluid" alt="...">
                      </div>
                      <div class="col-sm-8 col-md-8">
                        <div class="card-body">
                          <div
                            style="margin-top:30px;color:#499506;font-family:'Oswald', Arial, 'Helvetica Neue', Helvetica, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                            <div
                              style="line-height: 1.2; font-size: 12px; font-family: 'Oswald', Arial, 'Helvetica Neue', Helvetica, sans-serif; color: #499506; mso-line-height-alt: 14px;">
                              <p
                                style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: left; font-family: Oswald, Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 17px; margin: 0;">
                                <span style="color: #000000;"><span style="font-size: 28px;"><strong>Step 2:
                                    </strong></span><strong><span
                                      style="font-size: 28px; color: #008000;">CLEANSE</span></strong></span></p>
                              <p
                                style="font-size: 28px; line-height: 1.2; word-break: break-word; text-align: left; font-family: Oswald, Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 34px; margin: 0;">
                                <span style="color: #000000; font-size: 28px;"><strong>Age Reversist Exfoliating
                                    Cleanser</strong></span></p>
                            </div>
                          </div>
                          <div
                            style="color:#393d47;font-family:Lato, Tahoma, Verdana, Segoe, sans-serif;line-height:1.5;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                            <div
                              style="line-height: 1.5; font-size: 12px; color: #393d47; font-family: Lato, Tahoma, Verdana, Segoe, sans-serif; mso-line-height-alt: 18px;">
                              <p
                                style="line-height: 1.5; word-break: break-word; text-align: left; mso-line-height-alt: 20px; mso-ansi-font-size: 14px; margin: 0;">
                                <span>Gently yet deeply buff away
                                  dirt and dead cells leaving your complexion softer, brighter, and younger-looking. Use
                                  bi-weekly.</p>
                              <p
                                style="font-size: 22px; line-height: 1.5; word-break: break-word; text-align: left; mso-line-height-alt: 33px; margin: 0;">
                                <span style="font-size: 22px;"><strong>$29.90</strong></span></p>
                            </div>
                          </div>

                          <div align="left" class="button-container"
                            style="padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                            <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-spacing: 0; border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"><tr><td style="padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px" align="left"><v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="https://www.example.com" style="height:25.5pt; width:112.5pt; v-text-anchor:middle;" arcsize="0%" strokeweight="0.75pt" strokecolor="#000000" fillcolor="#000000"><w:anchorlock/><v:textbox inset="0,0,0,0"><center style="color:#f9f9f9; font-family:Arial, sans-serif; font-size:16px"><![endif]--><a
                              href="/product/dr-c-tuna-age-reversist-exfoliating-cleanser/12921"
                              style="-webkit-text-size-adjust: none; text-decoration: none; display: inline-block; color: #f9f9f9; background-color: #000000; border-radius: 0px; -webkit-border-radius: 0px; -moz-border-radius: 0px; width: auto; width: auto; border-top: 1px solid #000000; border-right: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; padding-top: 0px; padding-bottom: 0px; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; text-align: center; mso-border-alt: none; word-break: keep-all;"
                              target="_blank"><span
                                style="padding-left:15px;padding-right:15px;font-size:16px;display:inline-block;"><span
                                  style="font-size: 16px; line-height: 2; word-break: break-word; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; mso-line-height-alt: 32px;color: #FFFFFF;">SHOP
                                  NOW</span></span></a>
                            <!--[if mso]></center></v:textbox></v:roundrect></td></tr></table><![endif]-->
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="card mb-3">
                    <div class="row no-gutters">
                      <div class="col-sm-4 col-md-4" style="padding: 10px">
                        <img src="/AgeReversist/img/1104202_Tonic_400x400.jpg" class="card-img img-fluid" alt="...">
                      </div>
                      <div class="col-sm-8 col-md-8">
                        <div class="card-body">

                          <div
                            style=" margin-top:30px;color:#393d47;font-family:'Oswald', Arial, 'Helvetica Neue', Helvetica, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                            <div
                              style="line-height: 1.2; font-size: 12px; font-family: 'Oswald', Arial, 'Helvetica Neue', Helvetica, sans-serif; color: #393d47; mso-line-height-alt: 14px;">
                              <p
                                style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: left; font-family: Oswald, Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 17px; margin: 0;">
                                <span style="color: #000000;"><span style="font-size: 28px;"><strong>Step 3:
                                    </strong></span><strong><span
                                      style="font-size: 28px; color: #008000;">TONE</span></strong></span></p>
                              <p
                                style="font-size: 28px; line-height: 1.2; word-break: break-word; text-align: left; font-family: Oswald, Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 34px; margin: 0;">
                                <span style="color: #000000; font-size: 28px;"><strong>Age Reversist Beauty Essence
                                    Tonic</strong></span></p>
                            </div>
                          </div>

                          <div
                            style="color:#393d47;font-family:Lato, Tahoma, Verdana, Segoe, sans-serif;line-height:1.5;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                            <div
                              style="line-height: 1.5; font-size: 12px; color: #393d47; font-family: Lato, Tahoma, Verdana, Segoe, sans-serif; mso-line-height-alt: 18px;">
                              <p
                                style="line-height: 1.5; word-break: break-word; text-align: left; mso-line-height-alt: 20px; mso-ansi-font-size: 14px; margin: 0;">
                                <span>Soothes and balances skin while
                                  helping to remove post cleanse residue, for a daily silky-smooth debris-free finish.
                              </p>
                              <p
                                style="font-size: 22px; line-height: 1.5; word-break: break-word; text-align: left; mso-line-height-alt: 33px; margin: 0;">
                                <span style="font-size: 22px;"><strong>$29.90</strong></span></p>
                            </div>
                          </div>

                          <div align="left" class="button-container"
                            style="padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                            <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-spacing: 0; border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"><tr><td style="padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px" align="left"><v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="https://www.example.com" style="height:25.5pt; width:112.5pt; v-text-anchor:middle;" arcsize="0%" strokeweight="0.75pt" strokecolor="#000000" fillcolor="#000000"><w:anchorlock/><v:textbox inset="0,0,0,0"><center style="color:#f9f9f9; font-family:Arial, sans-serif; font-size:16px"><![endif]--><a
                              href="/product/dr-c-tuna-age-reversist-tonic/12922"
                              style="-webkit-text-size-adjust: none; text-decoration: none; display: inline-block; color: #f9f9f9; background-color: #000000; border-radius: 0px; -webkit-border-radius: 0px; -moz-border-radius: 0px; width: auto; width: auto; border-top: 1px solid #000000; border-right: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; padding-top: 0px; padding-bottom: 0px; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; text-align: center; mso-border-alt: none; word-break: keep-all;"
                              target="_blank"><span
                                style="padding-left:15px;padding-right:15px;font-size:16px;display:inline-block;"><span
                                  style="font-size: 16px; line-height: 2; word-break: break-word; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; mso-line-height-alt: 32px;color: #FFFFFF;">SHOP
                                  NOW</span></span></a>
                            <!--[if mso]></center></v:textbox></v:roundrect></td></tr></table><![endif]-->
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="card mb-3">
                    <div class="row no-gutters">
                      <div class="col-sm-4 col-md-4" style="padding: 10px">
                        <img src="/AgeReversist/img/1104199_Serum_400x400.jpg" class="card-img img-fluid" alt="...">
                      </div>
                      <div class="col-sm-8 col-md-8">
                        <div class="card-body">

                          <div
                            style="margin-top:30px;color:#393d47;font-family:'Oswald', Arial, 'Helvetica Neue', Helvetica, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                            <div
                              style="line-height: 1.2; font-size: 12px; font-family: 'Oswald', Arial, 'Helvetica Neue', Helvetica, sans-serif; color: #393d47; mso-line-height-alt: 14px;">
                              <p
                                style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: left; font-family: Oswald, Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 17px; margin: 0;">
                                <span style="color: #000000;"><span style="font-size: 28px;"><strong>Step 4:
                                    </strong></span></span>
                                <span style="color: #008000;"><span style=""><strong><span
                                        style="font-size: 28px;color: #008000;">TREAT</span></strong></span></span>
                              </p>
                              <p
                                style="font-size: 28px; line-height: 1.2; word-break: break-word; text-align: left; font-family: Oswald, Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 34px; margin: 0;">
                                <span style="color: #000000; font-size: 28px;"><strong>Age Reversist
                                    Serum</strong></span></p>
                            </div>
                          </div>



                          <div
                            style="color:#393d47;font-family:Lato, Tahoma, Verdana, Segoe, sans-serif;line-height:1.5;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                            <div
                              style="line-height: 1.5; font-size: 12px; color: #393d47; font-family: Lato, Tahoma, Verdana, Segoe, sans-serif; mso-line-height-alt: 18px;">
                              <p
                                style="line-height: 1.5; word-break: break-word; text-align: left; mso-line-height-alt: 20px; mso-ansi-font-size: 14px; margin: 0;">
                                <span>
                                  Nourishes the skin barrier and leaves it hydrated, supporting the skin`s natural
                                  power.
                              </p>
                              <p
                                style="font-size: 22px; line-height: 1.5; word-break: break-word; text-align: left; mso-line-height-alt: 33px; margin: 0;">
                                <span style="font-size: 22px;"><strong>$54.90</strong></span></p>
                            </div>
                          </div>

                          <div align="left" class="button-container"
                            style="padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                            <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-spacing: 0; border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"><tr><td style="padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px" align="left"><v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="https://www.example.com" style="height:25.5pt; width:112.5pt; v-text-anchor:middle;" arcsize="0%" strokeweight="0.75pt" strokecolor="#000000" fillcolor="#000000"><w:anchorlock/><v:textbox inset="0,0,0,0"><center style="color:#f9f9f9; font-family:Arial, sans-serif; font-size:16px"><![endif]--><a
                              href="/product/farmasi-dr-c-tuna-age-reversist-serum-30-ml-new-2/12900"
                              style="-webkit-text-size-adjust: none; text-decoration: none; display: inline-block; color: #f9f9f9; background-color: #000000; border-radius: 0px; -webkit-border-radius: 0px; -moz-border-radius: 0px; width: auto; width: auto; border-top: 1px solid #000000; border-right: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; padding-top: 0px; padding-bottom: 0px; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; text-align: center; mso-border-alt: none; word-break: keep-all;"
                              target="_blank"><span
                                style="padding-left:15px;padding-right:15px;font-size:16px;display:inline-block;"><span
                                  style="font-size: 16px; line-height: 2; word-break: break-word; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; mso-line-height-alt: 32px;color: #FFFFFF;">SHOP
                                  NOW</span></span></a>
                            <!--[if mso]></center></v:textbox></v:roundrect></td></tr></table><![endif]-->
                          </div>

                        </div>
                      </div>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="card mb-3">
                    <div class="row no-gutters">
                      <div class="col-sm-4 col-md-4" style="padding: 10px">
                        <img src="/AgeReversist/img/1104198_RichMoisturizer_400x400.jpg" class="card-img img-fluid" alt="...">
                      </div>
                      <div class="col-sm-8 col-md-8">
                        <div class="card-body">
                          <div
                            style="margin-top: 30px; color:#393d47;font-family:'Oswald', Arial, 'Helvetica Neue', Helvetica, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                            <div
                              style="line-height: 1.2; font-size: 12px; font-family: 'Oswald', Arial, 'Helvetica Neue', Helvetica, sans-serif; color: #393d47; mso-line-height-alt: 14px;">
                              <p
                                style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: left; font-family: Oswald, Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 17px; margin: 0;">
                                <span style="color: #000000;"><span style="font-size: 28px;"><strong>Step 5:
                                    </strong></span></span><strong><span
                                    style="font-size: 28px; color: #008000;">TREAT</span></strong></p>
                              <p
                                style="font-size: 26px; line-height: 1.2; word-break: break-word; text-align: left; font-family: Oswald, Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 31px; margin: 0;">
                                <span style="color: #000000; font-size: 26px;"><strong>Age Reversist Rich
                                    Moisturizer</strong></span></p>
                            </div>
                          </div>



                          <div
                            style="color:#393d47;font-family:Lato, Tahoma, Verdana, Segoe, sans-serif;line-height:1.5;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                            <div
                              style="line-height: 1.5; font-size: 12px; color: #393d47; font-family: Lato, Tahoma, Verdana, Segoe, sans-serif; mso-line-height-alt: 18px;">
                              <p
                                style="line-height: 1.5; word-break: break-word; text-align: left; mso-line-height-alt: 20px; mso-ansi-font-size: 14px; margin: 0;">
                                <span>Provides the skin with intense
                                  hydration, leaving it supple, smooth, and deeply nourished.</p>
                              <p
                                style="font-size: 22px; line-height: 1.5; word-break: break-word; text-align: left; mso-line-height-alt: 33px; margin: 0;">
                                <span style="font-size: 22px;"><strong>$29.90</strong></span></p>
                            </div>
                          </div>


                          <div align="left" class="button-container"
                            style="padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                            <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-spacing: 0; border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"><tr><td style="padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px" align="left"><v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="https://www.example.com" style="height:25.5pt; width:112.5pt; v-text-anchor:middle;" arcsize="0%" strokeweight="0.75pt" strokecolor="#000000" fillcolor="#000000"><w:anchorlock/><v:textbox inset="0,0,0,0"><center style="color:#f9f9f9; font-family:Arial, sans-serif; font-size:16px"><![endif]--><a
                              href="/product/dr-c-tuna-age-reversist-rich-moisturizer/12923"
                              style="-webkit-text-size-adjust: none; text-decoration: none; display: inline-block; color: #f9f9f9; background-color: #000000; border-radius: 0px; -webkit-border-radius: 0px; -moz-border-radius: 0px; width: auto; width: auto; border-top: 1px solid #000000; border-right: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; padding-top: 0px; padding-bottom: 0px; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; text-align: center; mso-border-alt: none; word-break: keep-all;"
                              target="_blank"><span
                                style="padding-left:15px;padding-right:15px;font-size:16px;display:inline-block;"><span
                                  style="font-size: 16px; line-height: 2; word-break: break-word; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; mso-line-height-alt: 32px;color: #FFFFFF;">SHOP
                                  NOW</span></span></a>
                            <!--[if mso]></center></v:textbox></v:roundrect></td></tr></table><![endif]-->
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="card mb-3">
                    <div class="row no-gutters">
                      <div class="col-sm-4 col-md-4" style="padding: 10px">
                        <img src="/AgeReversist/img/1104200_EyeCream_400x400.jpg" class="card-img img-fluid" alt="...">
                      </div>
                      <div class="col-sm-8 col-md-8">
                        <div class="card-body">
                          <div
                            style="margin-top:30px;color:#393d47;font-family:'Oswald', Arial, 'Helvetica Neue', Helvetica, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                            <div
                              style="line-height: 1.2; font-size: 12px; font-family: 'Oswald', Arial, 'Helvetica Neue', Helvetica, sans-serif; color: #393d47; mso-line-height-alt: 14px;">
                              <p
                                style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: left; font-family: Oswald, Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 17px; margin: 0;">
                                <span style="color: #000000;"><span style="font-size: 28px;"><strong>Step 6: <span
                                        style="color: #008000;">TREAT</span></strong></span></span></p>
                              <p
                                style="font-size: 26px; line-height: 1.2; word-break: break-word; text-align: left; font-family: Oswald, Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 31px; margin: 0;">
                                <span style="color: #000000; font-size: 26px;"><strong>Age Reversist Eye
                                    Cream</strong></span></p>
                            </div>
                          </div>
                          <div
                            style="color:#393d47;font-family:Lato, Tahoma, Verdana, Segoe, sans-serif;line-height:1.5;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                            <div
                              style="line-height: 1.5; font-size: 12px; color: #393d47; font-family: Lato, Tahoma, Verdana, Segoe, sans-serif; mso-line-height-alt: 18px;">
                              <p
                                style="line-height: 1.5; word-break: break-word; text-align: left; mso-line-height-alt: 20px; mso-ansi-font-size: 14px; margin: 0;">
                                <span>
                                  Helps to hydrate, soothe, and condition the eye area, helping to refine and replenish
                                  fine lines and deep wrinkles. </p>
                              <p
                                style="font-size: 22px; line-height: 1.5; word-break: break-word; text-align: left; mso-line-height-alt: 33px; margin: 0;">
                                <span style="font-size: 22px;"><strong>$24.90</strong></span></p>
                            </div>
                          </div>
                          <div align="left" class="button-container"
                            style="padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                            <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-spacing: 0; border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"><tr><td style="padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px" align="left"><v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="https://www.example.com" style="height:25.5pt; width:112.5pt; v-text-anchor:middle;" arcsize="0%" strokeweight="0.75pt" strokecolor="#000000" fillcolor="#000000"><w:anchorlock/><v:textbox inset="0,0,0,0"><center style="color:#f9f9f9; font-family:Arial, sans-serif; font-size:16px"><![endif]--><a
                              href="/product/dr-c-tuna-age-reversist-eye-cream/12924"
                              style="-webkit-text-size-adjust: none; text-decoration: none; display: inline-block; color: #f9f9f9; background-color: #000000; border-radius: 0px; -webkit-border-radius: 0px; -moz-border-radius: 0px; width: auto; width: auto; border-top: 1px solid #000000; border-right: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; padding-top: 0px; padding-bottom: 0px; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; text-align: center; mso-border-alt: none; word-break: keep-all;"
                              target="_blank"><span
                                style="padding-left:15px;padding-right:15px;font-size:16px;display:inline-block;"><span
                                  style="font-size: 16px; line-height: 2; word-break: break-word; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; mso-line-height-alt: 32px;color: #FFFFFF;">SHOP
                                  NOW</span></span></a>
                            <!--[if mso]></center></v:textbox></v:roundrect></td></tr></table><![endif]-->
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="card mb-3">
                    <div class="row no-gutters">
                      <div class="col-sm-4 col-md-4" style="padding: 10px">
                        <img src="/AgeReversist/img/1104201_AllNightBeautyMask_400x400.jpg" class="card-img img-fluid" alt="...">
                      </div>
                      <div class="col-sm-8 col-md-8">
                        <div class="card-body">
                          <div
                            style="margin-top:30px;color:#393d47;font-family:'Oswald', Arial, 'Helvetica Neue', Helvetica, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                            <div
                              style="line-height: 1.2; font-size: 12px; font-family: 'Oswald', Arial, 'Helvetica Neue', Helvetica, sans-serif; color: #393d47; mso-line-height-alt: 14px;">
                              <p
                                style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: left; font-family: Oswald, Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 17px; margin: 0;">
                                <span style="color: #000000;"><span style="font-size: 28px;"><strong>Step 7: <span
                                        style="color: #008000;">PROTECT </span></strong></span></span></p>
                              <p
                                style="font-size: 26px; line-height: 1.2; word-break: break-word; text-align: left; font-family: Oswald, Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 31px; margin: 0;">
                                <span style="color: #000000; font-size: 26px;"><strong>Age Reversist All Night Sleeping
                                    Mask</strong></span></p>
                            </div>
                          </div>
                          <div
                            style="color:#393d47;font-family:Lato, Tahoma, Verdana, Segoe, sans-serif;line-height:1.5;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                            <div
                              style="line-height: 1.5; font-size: 12px; color: #393d47; font-family: Lato, Tahoma, Verdana, Segoe, sans-serif; mso-line-height-alt: 18px;">
                              <p
                                style="line-height: 1.5; word-break: break-word; text-align: left; mso-line-height-alt: 20px; mso-ansi-font-size: 14px; margin: 0;">
                                <span>
                                  Dramatically enhances skin penetration with outstanding antioxidant properties, giving
                                  you the ultra-nourishment skin needs, turning back the hands of time while you sleep.
                              </p>
                              <p
                                style="font-size: 22px; line-height: 1.5; word-break: break-word; text-align: left; mso-line-height-alt: 33px; margin: 0;">
                                <span style="font-size: 22px;"><strong>$49.90</strong></span></p>
                            </div>
                          </div>
                          <div align="left" class="button-container"
                            style="padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                            <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-spacing: 0; border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"><tr><td style="padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px" align="left"><v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="https://www.example.com" style="height:25.5pt; width:112.5pt; v-text-anchor:middle;" arcsize="0%" strokeweight="0.75pt" strokecolor="#000000" fillcolor="#000000"><w:anchorlock/><v:textbox inset="0,0,0,0"><center style="color:#f9f9f9; font-family:Arial, sans-serif; font-size:16px"><![endif]--><a
                              href="/product/dr-c-tuna-age-reversist-all-night-beauty-mask/12920"
                              style="-webkit-text-size-adjust: none; text-decoration: none; display: inline-block; color: #f9f9f9; background-color: #000000; border-radius: 0px; -webkit-border-radius: 0px; -moz-border-radius: 0px; width: auto; width: auto; border-top: 1px solid #000000; border-right: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; padding-top: 0px; padding-bottom: 0px; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; text-align: center; mso-border-alt: none; word-break: keep-all;"
                              target="_blank"><span
                                style="padding-left:15px;padding-right:15px;font-size:16px;display:inline-block;"><span
                                  style="font-size: 16px; line-height: 2; word-break: break-word; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; mso-line-height-alt: 32px;color: #FFFFFF;">SHOP
                                  NOW</span></span></a>
                            <!--[if mso]></center></v:textbox></v:roundrect></td></tr></table><![endif]-->
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
    <footer class="footer-7 text-center-xs bg--dark">
      <div class="container">
        <div class="row">
          <div class="col-sm-6"> <span class="type--fine-print">&copy; <span class="update-year">2020</span> Farmasi</span>
          </div>
          <div class="col-sm-6 text-right text-center-xs">
            <ul class="social-list list-inline">
              <li><a href="https://www.youtube.com/channel/UCa0BWzAqhRML6yG5CRRBhNQ?view_as=subscriber"
                  target="_self"><i class="socicon icon socicon-youtube icon--xs"></i></a></li>
              <li><a href="https://www.facebook.com/Farmasi-USA-2234387646887335/?modal=admin_todo_tour"
                  target="_self"><i class="socicon socicon-facebook icon icon--xs"></i></a></li>
              <li><a href="https://www.instagram.com/farmasiusa/" target="_self"><i
                    class="socicon socicon-instagram icon icon--xs"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
  </div>
  <map name="newcomers-2021">
    <area target="_blank" alt="Exfoliating Cleanser" title="Exfoliating Cleanser" href="/product/dr-c-tuna-age-reversist-exfoliating-cleanser/12921" coords="154,164,477,577" shape="rect">
    <area target="_blank" alt="Beauty Essence Tonic" title="Beauty Essence Tonic" href="/product/dr-c-tuna-age-reversist-tonic/12922" coords="502,185,844,640" shape="rect">
    <area target="_blank" alt="Beauty Mask" title="Beauty Mask" href="/product/dr-c-tuna-age-reversist-all-night-beauty-mask/12920" coords="877,186,1216,592" shape="rect">
  </map>
  <script src="/AgeReversist/js/jquery-3.1.1.min.js"></script>
  <script src="/AgeReversist/js/flickity.min.js"></script>
  <script src="/AgeReversist/js/parallax.js"></script>
  <script src="/AgeReversist/js/smooth-scroll.min.js"></script>
  <script src="/AgeReversist/js/jquery.rwdImageMaps.min.js"></script>
  <script src="/AgeReversist/js/scripts.js"></script>
  <script>
    $(document).ready(function (e) {
      $('img[usemap]').rwdImageMaps();
    });
  </script>
</body>

</html>