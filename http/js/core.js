var menuOpen = function () {
  $('#menu').addClass('active');
}
var menuClose = function () {
  $('#menu').removeClass('active');
}
$('a.scroll').bind('click', function (e) {
  e.preventDefault();
  var t = $(this).attr('href');
  $('html, body').animate({
    scrollTop: $(t).offset().top
  }, 1000);
  menuClose();
});
$('#lang-wrapper .active').bind('click', function(e){
  e.preventDefault();
});